FROM trinitronx/python-simplehttpserver
COPY ./public/ /var/www/
EXPOSE 5000
CMD [ "python", "-m", "SimpleHTTPServer", "5000" ]